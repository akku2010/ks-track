import { Component, ViewChild, ElementRef, AfterViewInit, OnInit } from '@angular/core';
import { IonicPage, Events, NavController, NavParams, Content, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import * as moment from 'moment';
import * as io from 'socket.io-client';
// import { Keyboard } from '@ionic-native/keyboard';
import { ChatMessage, UserInfo } from "../../providers/chat-service";
// import { NavParams, IonContent, Events } from '@ionic/angular';
@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})

export class ChatPage implements AfterViewInit {
  // @ViewChild(Content) content: Content;

  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: ElementRef;
  // msgList: ChatMessage[] = [];
  user: UserInfo;
  toUser: UserInfo;
  editorMsg = '';
  showEmojiPicker = false;

  islogin: any;
  // fromDate: any;
  // toDate: string;
  paramData: any;
  userName: string;
  // User: any;
  // toUser: any;
  _io: any;
  loader: boolean;
  msgList: any = [];
  // user_input: string;
  // start_typing: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private plt: Platform,
  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.callBaseURL();
    // debugger
    if (navParams.get("isCustomer")) {
      if (this.navParams.get('params')) {
        this.paramData = this.navParams.get('params').Dealer_ID;
        this.userName = this.paramData.first_name;
        console.log("param data: ", this.paramData);
        this.toUser = {
          id: this.paramData._id,
          name: this.paramData.first_name
        };
        // Get mock user information
        this.user = {
          id: this.islogin._id,
          name: this.islogin.fn
        };
      }
    } else {
      if (this.navParams.get('params')) {
        this.paramData = this.navParams.get('params');
        this.userName = this.paramData.first_name;
        console.log("param data: ", this.paramData);
        this.toUser = {
          id: this.paramData._id,
          name: this.paramData.first_name
        };
        // Get mock user information
        this.user = {
          id: this.islogin._id,
          name: this.islogin.fn
        };
      }
    }
  }

  ionViewWillLeave() {
  }

  ngAfterViewInit() { }

  ionViewDidEnter() {
    //get message list
    this.getMsg();


    if (this.plt.is('ios')) {
      // this.keyboard.disableScroll(true);
    }

    window.addEventListener('native.keyboardshow', keyboardShowHandler);
    window.addEventListener('native.keyboardhide', keyboardHideHandler);
    window.addEventListener('touchstart', tapCoordinates);
    let y; let h; let offsetY;

    function tapCoordinates(e) {
      y = e.touches[0].clientY;
      h = window.innerHeight;
      offsetY = (h - y);
      console.log("offset = " + offsetY);
    }

    function keyboardShowHandler(e) {
      let kH = e.keyboardHeight;
      console.log(e.keyboardHeight);
      let bodyMove = <HTMLElement>document.querySelector("ion-app"), bodyMoveStyle = bodyMove.style;
      console.log("calculating " + kH + "-" + offsetY + "=" + (kH - offsetY));

      if (offsetY < kH + 40) {
        bodyMoveStyle.bottom = (kH - offsetY + 40) + "px";
        bodyMoveStyle.top = "initial";
      }
    }
    function keyboardHideHandler() {
      console.log('gone');
      let removeStyles = <HTMLElement>document.querySelector("ion-app");
      removeStyles.removeAttribute("style");
    }
  }



  onFocus(x) {
    x.style.background = "yellow";
    this.showEmojiPicker = false;
    this.content.resize();
    this.scrollToBottom();
  }

  switchEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
    if (!this.showEmojiPicker) {
      this.focus();
    } else {
      this.setTextareaScroll();
    }
    this.content.resize();
    this.scrollToBottom();
  }

  /**
   * @name getMsg
   * @returns {Promise<ChatMessage[]>}
   */
  getMsg() {
    // Get mock message list
    // return this.chatService
    //   .getMsgList()
    //   .subscribe(res => {

    //     this.msgList = res;
    //     this.scrollToBottom();
    //   });

    this.apiCall.startLoading().present();
    var url = this.apiCall.mainUrl + "broadcastNotification/getchatmsg?from=" + this.islogin._id + "&to=" + this.paramData._id;
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        debugger
        if (respData) {
          var res = respData;
          for (var i = 0; i < res.length; i++) {
            if (res[i].sender === this.toUser.id) {
              this.msgList.push({
                // userId: this.User,
                // userName: this.User,
                // time: res[i].timestamp,
                // message: res[i].message,
                // id: this.msgList.length + 1
                "messageId": this.msgList.length + 1,
                "userId": this.toUser.id,
                "userName": this.toUser.name,
                // "userImgUrl": "./assets/user.jpg",
                "toUserId": this.user.id,
                "toUserName": this.user.name,
                // "userAvatar": "./assets/to-user.jpg",
                "time": res[i].timestamp,
                "message": res[i].message,
                "status": "success"
              })
            } else {
              if (res[i].sender === this.user.id) {
                this.msgList.push({
                  // userId: this.toUser,
                  // userName: this.toUser,
                  // time: res[i].timestamp,
                  // message: res[i].message,
                  // id: this.msgList.length + 1
                  "messageId": this.msgList.length + 1,
                  "userId": this.user.id,
                  "userName": this.user.name,
                  // "userImgUrl": "./assets/user.jpg",
                  "toUserId": this.toUser.id,
                  "toUserName": this.toUser.name,
                  // "userAvatar": "./assets/to-user.jpg",
                  "time": res[i].timestamp,
                  "message": res[i].message,
                  "status": "success"
                })
              }
            }
          }

          setTimeout(() => {
            this.scrollToBottom();
            // this.content.scrollToBottom(100);
          }, 50);

          // this.scrollDown();
        }
      },
        err => {
          this.apiCall.stopLoading();
          console.log("chat err: ", err)
        });
  }

  /**
   * @name sendMsg
   */
  sendMsg() {
    if (!this.editorMsg.trim()) return;
    let that = this;
    that._io.emit('send', this.user.id, this.toUser.id, this.editorMsg);   // three parameters, from(who is sending msg), to(to whom ur sending msg), msg(message string)

    // Mock message
    const id = Date.now().toString();
    let newMsg: ChatMessage = {
      messageId: this.msgList.length + 1,
      userId: this.user.id,
      userName: this.user.name,
      // userAvatar: this.user.avatar,
      toUserId: this.toUser.id,
      time: new Date().toISOString(),
      message: this.editorMsg,
      status: 'success'
    };

    this.pushNewMsg(newMsg);
    this.editorMsg = '';

    if (!this.showEmojiPicker) {
      this.focus();
    }
  }

  /**
   * @name pushNewMsg
   * @param msg
   */
  pushNewMsg(msg: ChatMessage) {
    const userId = this.user.id,
      toUserId = this.toUser.id;
    // Verify user relationships
    if (msg.userId === userId && msg.toUserId === toUserId) {
      this.msgList.push(msg);
    } else if (msg.toUserId === userId && msg.userId === toUserId) {
      this.msgList.push(msg);
    }
    this.scrollToBottom();
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.messageId === id)
  }

  scrollToBottom() {
    let that = this;
    setTimeout(() => {
      if (that.content.scrollToBottom) {
        that.content.scrollToBottom();
      }
    }, 400)
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  private setTextareaScroll() {
    const textarea = this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
  }


  ngOnInit() {
    this.openChatSocket();
    // this.getChatHistory();
    // this.innerWidth = window.innerWidth;
    // console.log("window test: ", this.innerWidth)
  }

  callBaseURL() {
    // debugger
    if (localStorage.getItem("ENTERED_BASE_URL") === null) {
      let url = "https://www.oneqlik.in/pullData/getUrlnew";
      this.apiCall.getSOSReportAPI(url)
        .subscribe((data) => {
          console.log("base url: ", data);
          if (data.url) {
            localStorage.setItem("BASE_URL", JSON.stringify(data.url));
          }
          if (data.socket) {
            localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
          }
          this.getSocketUrl();
        });
    }
  }
  socketurl: any;
  getSocketUrl() {
    if (localStorage.getItem('SOCKET_URL') !== null) {
      this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
      this._io = io(this.socketurl + '/userChat', {
        transports: ['websocket']
      });
      this._io.on('connect', (data) => {
        console.log("userChat connect data: ", data);
      });
    }
  }
  openChatSocket() {

    let that = this;
    that._io.on(that.user.id + "-" + that.toUser.id, (d4) => {
      if (d4 != undefined)
        (function (data) {
          if (data == undefined) {
            return;
          }
          that.loader = true;
          setTimeout(() => {
            that.msgList.push({
              // userId: that.user.id,
              // userName: that.User,
              // // userAvatar: "../../assets/chat/chat5.jpg",
              // time: new Date().toISOString(),
              // message: data

              messageId: that.msgList.length + 1,
              userId: that.toUser.id,
              userName: that.toUser.name,
              // userAvatar: this.user.avatar,
              toUserId: that.user.id,
              time: new Date().toISOString(),
              message: data,
              status: 'success'
            });
            that.loader = false;
            // that.scrollDown();
            that.scrollToBottom();
          }, 2000)
          // that.scrollDown();
        })(d4)
    })
  }


}
