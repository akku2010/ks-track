import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImmobilizePage } from './immobilize';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ImmobilizePage,
  ],
  imports: [
    IonicPageModule.forChild(ImmobilizePage),
    TranslateModule.forChild()
  ]
})
export class ImmobilizePageModule {}
