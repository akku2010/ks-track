import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';

@IonicPage()
@Component({
  selector: 'page-loading-unloading-trip',
  templateUrl: 'loading-unloading-trip.html',
})
export class LoadingUnloadingTripPage {
  islogin: any;
  tripData: any = [];
  pltStr: string;

  constructor(
    public file: File,
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private plt: Platform
  ) {
    if(this.plt.is('android')){
      this.pltStr = 'md';
    } else if(this.plt.is('ios')) {
      this.pltStr = 'ios';
    }
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter LoadingUnloadingTripPage');
    this.getReport();
    this.getDefaultUserSettings();
  }
  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }

  getReport() {
    this.tripData = [];
    var url = this.apiCall.mainUrl + "trackRouteMap/getTripReport?userid=" + this.islogin._id;
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(url).subscribe((resp) => {
      this.apiCall.stopLoading();
      console.log("trip data: ", resp);
      if (resp) {
        // this.tripData = resp;
        debugger
        for (var i = 0; i < resp.length; i++) {
          this.tripData.push(
            {
              _id: resp[i]._id,
              tripType: resp[i].tripType,
              status: resp[i].status,
              Source: {
                _id: resp[i].Source._id,
                geoname: resp[i].Source.geoname,
                status: resp[i].Source.status
              },
              Destination: {
                _id: resp[i].Destination._id,
                geoname: resp[i].Destination.geoname,
                status: resp[i].Destination.status
              },
              vrn: resp[i].vrn,
              loadingPoint: {
                reportingDateTime: resp[i].loadingPoint.reportingDateTime,
                exitDateTime: resp[i].loadingPoint.exitDateTime,
                halting: resp[i].loadingPoint.halting,
              },
              destination: {
                reportingDateTime: resp[i].destination.reportingDateTime,
                exitDateTime: resp[i].destination.exitDateTime,
                halting: resp[i].destination.halting,
              },
              returnToLoadingPoint: {},
              tripTime: {
                total: (resp[i].tripTime.total ? this.convertSecondIntoHM(resp[i].tripTime.total) : '0:0'),
                stoppage: (resp[i].tripTime.stoppage ? this.convertSecondIntoHM(resp[i].tripTime.stoppage) : '0:0'),
                running: (resp[i].tripTime.running ? this.convertSecondIntoHM(resp[i].tripTime.running) : '0:0'),
                idle: (resp[i].tripTime.idle ? this.convertSecondIntoHM(resp[i].tripTime.idle) : '0:0')
              },
              distanceTravelled: (resp[i].distanceTravelled ? resp[i].distanceTravelled : '0')
            }
          )
        }
      }
    },
      err => {
        this.apiCall.stopLoading();
        console.log("error: ", err);
      });
  }

  convertSecondIntoHM(totalSeconds) {
    var hours = Math.floor(totalSeconds / 3600);
    var temp = totalSeconds % 3600;
    var minutes = Math.floor(temp / 60);
    // var seconds = temp % 60;

    var output = hours + ":" + minutes;
    console.log("conertion: ", output);
    return output;
  }

  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.tripData);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "loading_unloading_trip_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

}
