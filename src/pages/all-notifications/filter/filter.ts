import { Component, OnInit } from "@angular/core";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { ViewController } from "ionic-angular";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-filter',
  templateUrl: './filter.html'
})
export class FilterPage implements OnInit {
  modal: any = {};
  filterList: any = [
    { filterID: 1, filterName: this.translate.instant('Overspeeding'), filterValue: 'overspeed', checked: false },
    { filterID: 2, filterName: this.translate.instant('Ignition'), filterValue: 'IGN', checked: false },
    { filterID: 3, filterName: "Route", filterValue: 'Route', checked: false },
    { filterID: 4, filterName: this.translate.instant('GEOFENCE'), filterValue: 'Geo-Fence', checked: false },
    { filterID: 5, filterName: this.translate.instant('Stoppage'), filterValue: 'MAXSTOPPAGE', checked: false },
    { filterID: 6, filterName: this.translate.instant('Fuel'), filterValue: 'Fuel', checked: false },
    { filterID: 7, filterName: this.translate.instant("AC"), filterValue: 'AC', checked: false },
    { filterID: 8, filterName: "Route POI", filterValue: 'route-poi', checked: false },
    { filterID: 9, filterName: this.translate.instant('Power'), filterValue: 'power', checked: false },
    { filterID: 10, filterName: this.translate.instant('Immobilize'), filterValue: 'immo', checked: false },
    { filterID: 11, filterName: "Parking", filterValue: 'theft', checked: false },
    { filterID: 12, filterName: this.translate.instant('SOS'), filterValue: 'SOS', checked: false },
    { filterID: 13, filterName: 'Reminder', filterValue: 'Reminder', checked: false }
  ]
  selectedArray: any = [];
  islogin: any;
  vehicleList: any;
  checkedData: any;


  constructor(
    public apiCall: ApiServiceProvider,
    public viewCtrl: ViewController,
    private translate: TranslateService
    // private datePicker: DatePicker
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + this.islogin);

    if (localStorage.getItem("checkedList") != null) {
      this.checkedData = JSON.parse(localStorage.getItem("checkedList"));
      console.log("checked data=> " + this.checkedData)
    }
  }
  ngOnInit() {
    // this.getNotifTypes()
  }

  getNotifTypes() {
    this.apiCall.startLoading();
    this.apiCall.getVehicleListCall(this.apiCall.mainUrl + "notifs/getTypes")
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("notif type list: ", data);
        this.filterList = data;
      })
  }

  applyFilter() {
    localStorage.setItem("checkedList", JSON.stringify(this.selectedArray));

    if (this.selectedArray.length == 0) {
      localStorage.setItem("dates", "dates")
      this.viewCtrl.dismiss(this.modal);
    } else {
      localStorage.setItem("types", "types")
      this.viewCtrl.dismiss(this.selectedArray);
    }

  }
  close() {
    this.viewCtrl.dismiss();
  }

  selectMember(data) {
    console.log("selected=> " + JSON.stringify(data))

    if (data.checked == true) {
      this.selectedArray.push(data);
    } else {
      let newArray = this.selectedArray.filter(function (el) {
        return el.filterID !== data.filterID;
      });
      this.selectedArray = newArray;
    }

  }

  cancel() {
    console.log("do nothing");
    this.viewCtrl.dismiss();
  }

}
